(function() {
	"use strict";

	function uaMatch( ua ) {
		// If an UA is not provided, default to the current browser UA.
		if ( ua === undefined ) {
			ua = window.navigator.userAgent;
		}
		ua = ua.toLowerCase();

		var match = /(edge)\/([\w.]+)/.exec( ua ) ||
				/(opr)[\/]([\w.]+)/.exec( ua ) ||
				/(chrome)[ \/]([\w.]+)/.exec( ua ) ||
				/(iemobile)[\/]([\w.]+)/.exec( ua ) ||
				/(version)(applewebkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
				/(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
				/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
				/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
				/(msie) ([\w.]+)/.exec( ua ) ||
				ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
				ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
				[];

		var platform_match = /(ipad)/.exec( ua ) ||
				/(ipod)/.exec( ua ) ||
				/(windows phone)/.exec( ua ) ||
				/(iphone)/.exec( ua ) ||
				/(kindle)/.exec( ua ) ||
				/(silk)/.exec( ua ) ||
				/(android)/.exec( ua ) ||
				/(win)/.exec( ua ) ||
				/(mac)/.exec( ua ) ||
				/(linux)/.exec( ua ) ||
				/(cros)/.exec( ua ) ||
				/(playbook)/.exec( ua ) ||
				/(bb)/.exec( ua ) ||
				/(blackberry)/.exec( ua ) ||
				[];

		var browser = {},
				matched = {
					browser: match[ 5 ] || match[ 3 ] || match[ 1 ] || "",
					version: match[ 2 ] || match[ 4 ] || "0",
					versionNumber: match[ 4 ] || match[ 2 ] || "0",
					platform: platform_match[ 0 ] || ""
				};

		if ( matched.browser ) {
			browser[ matched.browser ] = true;
			browser.version = matched.version;
			browser.versionNumber = parseInt(matched.versionNumber, 10);
		}

		if ( matched.platform ) {
			browser[ matched.platform ] = true;
		}

		// These are all considered mobile platforms, meaning they run a mobile browser
		if ( browser.android || browser.bb || browser.blackberry || browser.ipad || browser.iphone ||
			browser.ipod || browser.kindle || browser.playbook || browser.silk || browser[ "windows phone" ]) {
			browser.mobile = true;
		}

		// These are all considered desktop platforms, meaning they run a desktop browser
		if ( browser.cros || browser.mac || browser.linux || browser.win ) {
			browser.desktop = true;
		}

		// Chrome, Opera 15+ and Safari are webkit based browsers
		if ( browser.chrome || browser.opr || browser.safari ) {
			browser.webkit = true;
		}

		// IE11 has a new token so we will assign it msie to avoid breaking changes
		if ( browser.rv || browser.iemobile) {
			var ie = "msie";

			matched.browser = ie;
			browser[ie] = true;
		}

		// Edge is officially known as Microsoft Edge, so rewrite the key to match
		if ( browser.edge ) {
			delete browser.edge;
			var msedge = "msedge";

			matched.browser = msedge;
			browser[msedge] = true;
		}

		// Blackberry browsers are marked as Safari on BlackBerry
		if ( browser.safari && browser.blackberry ) {
			var blackberry = "blackberry";

			matched.browser = blackberry;
			browser[blackberry] = true;
		}

		// Playbook browsers are marked as Safari on Playbook
		if ( browser.safari && browser.playbook ) {
			var playbook = "playbook";

			matched.browser = playbook;
			browser[playbook] = true;
		}

		// BB10 is a newer OS version of BlackBerry
		if ( browser.bb ) {
			var bb = "blackberry";

			matched.browser = bb;
			browser[bb] = true;
		}

		// Opera 15+ are identified as opr
		if ( browser.opr ) {
			var opera = "opera";

			matched.browser = opera;
			browser[opera] = true;
		}

		// Stock Android browsers are marked as Safari on Android.
		if ( browser.safari && browser.android ) {
			var android = "android";

			matched.browser = android;
			browser[android] = true;
		}

		// Kindle browsers are marked as Safari on Kindle
		if ( browser.safari && browser.kindle ) {
			var kindle = "kindle";

			matched.browser = kindle;
			browser[kindle] = true;
		}

		 // Kindle Silk browsers are marked as Safari on Kindle
		if ( browser.safari && browser.silk ) {
			var silk = "silk";

			matched.browser = silk;
			browser[silk] = true;
		}

		// Assign the name and platform variable
		browser.name = matched.browser;
		browser.platform = matched.platform;
		return browser;
	}

	window.jQBrowser = uaMatch( window.navigator.userAgent );
	window.jQBrowser.uaMatch = uaMatch;

	return window.jQBrowser;
})();

if (jQBrowser.msedge || jQBrowser.msie) {
	document.body.classList.add('browser-ms');
};
if (jQBrowser.safari) {
	document.body.classList.add('safari');
};

var bodyOverflow = (function (body) {
	var scrollTop;
	return {
		fixBody: function () {
			scrollTop = body.scrollTop;
			body
				.classList.add('fixed');
			body
				.style.top = -scrollTop;
		},
		unfixBody: function () {
			body
				.classList.remove('fixed');
			body
				.scrollTop = scrollTop;
		}
	};
})(document.body);

function useTile(tile) {

	tile.innerHTML = '<div class="tile-holder">' + tile.innerHTML + '<div class="blick"><div><div>';

	var options = {
		'power': 20,
		'defSpeed': 0,
		'duration': 2000,
		'translateZ': 10
	}

	if (parseInt(jQBrowser.version) < 9 && jQBrowser.safari) {
		options.power = 1;
	}
	if (jQBrowser.chrome) {
		options.defSpeed = 0.1;
	}
	if (jQBrowser.chrome) {
		options.defSpeed = 0.1;
	}

	var plg = {
		init: function (tile) {
			this.state = {};
			this.tile = tile;
			this.plate = this.tile.querySelector('.tile-holder');
			this.blick = this.tile.querySelector('.blick');

			this.resize();

			this.tile
				.addEventListener('mouseenter', this.mouseenter.bind(this));
			this.tile
				.addEventListener('mousemove', this.mousemove.bind(this));
			this.tile
				.addEventListener('mouseleave', this.mouseleave.bind(this));

		},
		resize: function () {
			if (this.resizing) {
				clearInterval(this.resizing);
			}
			this.resizing = setTimeout(function () {
				this.resizing = false;
				var width = getComputedStyle(this.tile).width;
				width = parseInt(width);
				var height = getComputedStyle(this.tile).height;
				height = parseInt(height);
				this.state.elementWidth = width;
				this.state.elementHeight = height;
			}.bind(this), 100);
		},
		mouseenter: function (e) {

			var offset = this.tile.getBoundingClientRect();

			plg.animationState = {
				startStamp: new Date().getTime(),
				active: true,
				startX: 0.5,
				startY: 0.5,
				speed: 0.6,
				endX: (e.pageX - offset.left) / this.state.elementWidth,
				endY: (e.pageY - offset.top) / this.state.elementWidth,
				status: 0
			};

			plg.animationState.startX = plg.animationState.stateX || 0.5;
			plg.animationState.startY = plg.animationState.stateY || 0.5;

		},
		mousemove: function (e) {

			var offset = this.tile.getBoundingClientRect();

			var xmult = (e.clientX - offset.left) / this.state.elementWidth;
			var ymult = (e.clientY - offset.top) / this.state.elementHeight;
			var currentTime = new Date().getTime();

			if (!(jQBrowser.chrome || jQBrowser.msie || jQBrowser.msedge) && currentTime - plg.animationState.startStamp < 300 ) {
				plg.animationState.speed = (300 - (currentTime - plg.animationState.startStamp)) / 1000;
			} else {
				plg.animationState.speed = options.defSpeed;
			}

			plg.renderElement(xmult, ymult);

		},
		mouseleave: function (e) {

			var offset = this.tile.getBoundingClientRect();

			plg.animationState.startX = (e.pageX - offset.left) / this.state.elementWidth;
			plg.animationState.startY = (e.pageY - offset.top) / this.state.elementWidth;
			plg.animationState.status = 0;
			plg.animationState.speed = 0.6;
			plg.animationState.endX = 0.5;
			plg.animationState.endY = 0.5;
			plg.animationState.last = true;

			plg.renderElement(plg.animationState.endX, plg.animationState.endY);
		},
		renderElement: function (x, y, z) {

			if (!z) {
				z = options.translateZ;
			}

			var plate = this.plate;
			var blick = this.blick;

			window.requestAnimationFrame( function () {

plate.style.transition =
'transform ' + plg.animationState.speed + 's linear';
plate.style.transform =
'rotateY(' + (-(x - 0.5) * -options.power) + 'deg) rotateX(' + (-(y - 0.5) * options.power) + 'deg) translateZ(' + z + 'px)';
blick.style.backgroundImage =
'linear-gradient(' + (x * 25 + 2) + 'deg, transparent, rgba(255, 255, 255, 0.2) ' + (y * 30 + 40) + '%, transparent ' + (y * 30 + 100) + '%)';

			} );

		}
	};

	plg.init(tile);

	window.addEventListener('resize', plg.resize.bind(plg));
}

// modals
(function () {
	var triggers = document.querySelectorAll('[data-modal]');

	var clickTrigger = function (e) {
		try {
			e.preventDefault();
			var modalSelector = this.dataset.modal,
				modal;
			if (!modalSelector) {
				console.warn("modal selector is " + modalSelector);
				return;
			}
			modal = document.querySelector(modalSelector);
			if (!modal) {
				console.warn("modal not found " + modalSelector);
				return;
			}
			modal.classList.add('opening');
			setTimeout(function () {
				modal.classList.add('opened');
			}, 1)
			setTimeout(function () {
				bodyOverflow.fixBody();
				modal.classList.remove('opening');
			}, 300)
			if (!modal.dataset.enabled) {
				modal.addEventListener('click', closeTrigger);
				modal.dataset.enabled = 1;
			}
		} catch (error) {
			console.error(error);
		}

	},
	closeTrigger = function (e) {
		var target = e.target;
		if (this === target || target.classList.contains('close-modal')) {
			this.classList.add('clothing');
			bodyOverflow.unfixBody();
			setTimeout(function () {
				this.classList.remove('clothing');
				this.classList.remove('opened');
			}.bind(this), 300)
		}
	};

	for (var i = 0; i < triggers.length; i++) {
		triggers[i].addEventListener('click', clickTrigger);
	}
	window.addEventListener('keyup', function (e) {
		if (e.keyCode === 27) {
			var target = document.querySelector('.modal-window.opened')
			target.classList.add('clothing');
			bodyOverflow.unfixBody();
			setTimeout(function () {
				target.classList.remove('clothing');
				target.classList.remove('opened');
			}.bind(this), 300)
		};
	});

})();


window.addEventListener('load', function () {
	var tiles = document.querySelectorAll('.portfolio-block__item');
	for (var i = 0; i < tiles.length; i++) {
		useTile(tiles[i]);
	}
	useTile( document.querySelector('.brand-label') );
	// useTile( document.querySelector('.services-block__header') );

	var tables = document.querySelectorAll('table');
	for (var s = 0; s < tables.length; s++) {
		tables[s].outerHTML = '<div class="table-holder">' + tables[s].outerHTML + '</div>';
	}
});

